# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-03-30 00:47+0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)\n"

#: models.py:7
msgid "smile"
msgstr "смайл"

#: models.py:8
msgid "smiles"
msgstr "смайлы"

#: models.py:13
msgid "name"
msgstr "имя"

#: models.py:19
msgid "text"
msgstr "текст"

#: models.py:24 models.py:36 models.py:116 models.py:164
msgid "image"
msgstr "изображение"

#: models.py:40
msgid "uploaded file"
msgstr "загруженный файл"

#: models.py:41
msgid "uploaded files"
msgstr "загруженные файлы"

#: models.py:47 models.py:111
msgid "filename"
msgstr "имя файла"

#: models.py:52
msgid "body"
msgstr "содержимое"

#: models.py:60 models.py:129
msgid "user"
msgstr "пользователь"

#: models.py:65 models.py:134
msgid "create time"
msgstr "время создания"

#: models.py:71 models.py:140
msgid "mime type"
msgstr "тип MIME"

#: models.py:76 models.py:145
msgid "file length"
msgstr "размер файла"

#: models.py:94 models.py:163
msgid "URL"
msgstr "URL"

#: models.py:95 models.py:165
msgid "file size"
msgstr "размер файла"

#: models.py:97
msgid "File"
msgstr "Файл"

#: models.py:104
msgid "uploaded image"
msgstr "загруженная картинка"

#: models.py:105
msgid "uploaded images"
msgstr "загруженные картинки"

#: models.py:121
msgid "thumbnail"
msgstr "миниатюра"

#: templates/wysibb/init.html:75 templates/wysibb/init.html.py:78
msgid "Add file"
msgstr "добавить файл"

#: templatetags/bb_parser.py:146
msgid "said"
msgstr "сказал(а)"
