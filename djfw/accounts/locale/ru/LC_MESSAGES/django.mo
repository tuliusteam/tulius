��    $      <      \      \  )   ]     �     �  A   �  -   �          )     /     8     I     e  :        �     �     �  _   �     J  "   R  %   u     �  N   �  F        J     X  o   a     �  '   �          (     .     5     >     S     i     n  �  w  O   L  3   �     �  |   �  e   T	  1   �	  
   �	     �	     
  4   !
  8   V
  n   �
  _   �
  $   ^     �  ~   �  $     <   >  >   {  =   �  �   �  p   �  +   �     '  �   G  0   -  \   ^     �  
   �  
   �     �  %   �  %         F     _   A user with that username already exists. Activate users E-mail Great! Now you can simply log in using your username and password I have read and agree to the Terms of Service Invalid login/password pair Login Password Password (again) Profile activation complete Profile activation failed Provided activation key doesn't seem to be correct, sorry. Re-send activation emails Register Registration Registration using free email addresses is prohibited. Please supply a different email address. Sign up Successful registration completion The two password fields didn't match. This account is disabled This email address is already in use. Please supply a different email address. This value may contain only letters, numbers and @/./+/-/_ characters. Use this link Username We've send an activation link to provided e-mail. Check your mailbox, we hope to see you logged in very soon ;) You have successfully logged in You must agree to the terms to register activation key login logout password registration profile registration profiles user username Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-04-27 15:48+0400
PO-Revision-Date: 2012-04-27 15:53
Last-Translator:   <admin@example.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
X-Translated-Using: django-rosetta 0.6.6
 Пользователь с таким именем уже существует Активировать пользователей E-mail Ура! Теперь вы можете просто войти на сайт, используя логин и пароль. Я прочитал и согласен с Условиями Предоставления Услуг Неверная пара логин/пароль Войти Пароль Пароль (ещё раз) Активация профиля завершена Активация профиля провалилась Предоставленный вами ключ активации не является корректным Повторить отправку писем с активационными ссылками Зарегистрироваться Регистрация Этот адрес запрещён для регистрации. Пожалуйста, используйте другой. Зарегистрироваться Успешное завершение регистрации Два введённых пароля не совпадают Эта учётная запись заблокирована Этот адрес электронной почты уже зарегистрирован. Пожалуйста, выберите другой. Допустимые символы для этого поля: латинские буквы, цифры, @.+-_ Используйте эту ссылку: Имя пользователя Мы послали вам на указанную почту активационную ссылку. Проверьте свой почтовый ящик - мы надеемся вскоре увидеть вас вновь ;) Вы успешно авторизовались Для регистрации вы должны согласиться с Условиями ключ активации войти выйти пароль профиль регистрации профили регистрации пользователь имя пользователя 